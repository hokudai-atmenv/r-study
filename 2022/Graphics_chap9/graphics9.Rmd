---
title: "9"
author: "kazuki tanaka"
date: "2021/10/24"
output: html_document
---
# 9章　グラフの全体的な体裁

この章では、ggplot2で作った図の全体的な体裁を整える方法を解説します。

## 9.1 グラフのタイトルを設定する。

グラフのタイトルを設定したい。
  
```{r}
library(tidyverse)
library(gcookbook)
p<- ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point()
p+ ggtitle("Age and Height of Schoolchildren")
# 改行には \n を使う
p+ ggtitle("Age and Height\nof Schoolchildren")
```

ggtitle()は、labs(title = "Title text")と同じ。

ここで、タイトルをプロット領域内に移したいとき、2つの方法があります。
1つめは、ggtitle()と負のvjust値を使う方法で、2つめはタイトルの代わりにテキスト注釈を使う方法です。

```{r}
# タイトルを中に移す
p+ ggtitle("Age and Height of Schoolchildren") + theme(plot.title=element_text(vjust= -2.5))


# テキスト注釈をタイトルの代わりに使う。
p+ annotate("text",x=mean(range(heightweight$ageYear)), y=Inf,
label="Age and Height of Schoolchildren", vjust=1.5, size=6)

```


## 9.2 テキストの体裁を変更する

　図中にあるテキストの体裁を変更したい。
　タイトル、軸ラベル、軸メモリなど、テーマ要素の体裁を指定するには、theme()を使いelement()_text()で要素を設定します。
　

```{r} 
library(gcookbook)
# 基本プロット
p<- ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point()
# テーマ要素の体裁を指定する
p+ theme(axis.title.x=element_text(size=16, lineheight=.9, family="Times", face="bold.italic", colour="red"))
p+ ggtitle("Age and Height\nof Schoolchildren") + theme(plot.title=element_text(size=rel(1.5), lineheight=.9, family="Times", face="bold.italic", colour="red"))
```

## 9.2 テーマを使う

組み込みのテーマを使って、プロット全体の体裁を変更したい。
組み込みのテーマを使うにはtheme_bw()やtheme_grey()を追加します。

```{r}
library(gcookbook) 
# 基本プロット 
p<- ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point()
# グレーのテーマ（デフォルト）
p+ theme_grey()
# ブラックとホワイトのテーマ
p+ theme_bw()
```

ggplot2のテーマ要素に共通のプロパティのいくつかは、theme()で変更できる。また、theme_bw()を使うことによって、Rセッションに対するデフォルトテーマを設定できます。

## 9.4　テーマ要素の体裁を変更する。

テーマ要素の体裁を変更したい。

```{r}
 library(gcookbook) 
# 基本プロット 
p<- ggplot(heightweight, aes(x=ageYear, y=heightIn, colour=sex)) + geom_point()
# プロット領域の設定 
p+ theme( panel.grid.major= element_line(colour="red"), panel.grid.minor= element_line(colour="red", linetype="dashed", size=0.2), panel.background= element_rect(fill="lightblue"), panel.border= element_rect(colour="blue", fill=
NA
, size=2))
# テキスト項目の設定
p+ ggtitle("Plot title here") + theme( axis.title.x= element_text(colour="red", size=14), axis.text.x= element_text(colour="blue"), axis.title.y= element_text(colour="red", size=14, angle= 90), axis.text.y= element_text(colour="blue"), plot.title= element_text(colour="red", size=20, face="bold"))
# 凡例の設定 
p+ theme( legend.background= element_rect(fill="grey85", colour="red", size=1), legend.title= element_text(colour="blue", face="bold", size=14),legend.text= element_text(colour="red"), legend.key= element_rect(colour="blue", size=0.25))
# ファセットの設定
p+ facet_grid(sex ~.) + theme( strip.background= element_rect(fill="pink"), strip.text.y= element_text(size=14, angle=-90, face="bold")) 
# strip.text.xは推定方向ファセット用
```

既存のテーマを使いつつ部分的にtheme()で調整したいときには、テーマ指定の後にtheme()を置く必要がある。そうしないと、theme()での設定が既存のテーマによりすべて上書きされてしまう。

## 9.5 独自のテーマを作成する。

独自のテーマを作成したい。
既存のテーマに要素を追加することで独自のテーマを作成することができます。

```{r}
library(gcookbook) 
# theme_bw()を基にしていくつか変更する
mytheme<- theme_bw() + theme(text= element_text(colour="red"), axis.title= element_text(size= rel(1.25)))
# 基本プロット 
p<- ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point()
# 変更したテーマを使ってプロットする
p+ mytheme
```

ggplot2では、デフォルトテーマを使用することができるだけでなく、必要に応じて変更することもできます。テーマ要素を追加したり、既存要素の値を変更することができます

## 9.6 目盛り線を消す

プロット内の目盛り線を消したい
主目盛り線はpanel.grid.majorで制御できます。
補助目盛り線はpanel.grid.minorで制御できます。

```{r}
library(gcookbook) 
p<- ggplot(heightweight, aes(x=ageYear, y=heightIn)) + geom_point()
p+ theme(panel.grid.major= element_blank(), panel.grid.minor= element_blank())
```

panel.grid.major.x,panel.grid.major.y,panel.grid.minor.x、panel.grid.minor.yを使って、垂直方向、もしくは水平方向の目盛り線だけを消すこともできます。
: