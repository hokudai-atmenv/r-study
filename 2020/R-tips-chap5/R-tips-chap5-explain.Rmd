---
title: "R-Tips 第5章"
author: "Junta Tagusari"
date: "2020/10/26"
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: false
editor_options: 
  chunk_output_type: console
---

# 5.1 関数とは

入力（引数）と出力（返り値）をもつのが関数．

たとえば，
```{r}
log10(1e5)
```

ただし，入力をもたない関数もある．
```{r}
Sys.Date()
```

# 5.2 関数定義の基本

たとえば，関数`cmptX`は次の様に定義できる．

```{r}
cmptX <- function(arg1, arg2, arg3){ #arg1, arg2, arg3は引数．
  ret1 <- arg1 + arg2 #引数を使った処理
  ret2 <- ret1 - arg3 #引数を使った処理
  return(ret2) #返り値
}

cmptX(5,10,2)
```

## 練習問題

 1. $x^2$二乗が返り値の関数`mypower01(x)`
    ```{r}
    mypower01 <- function(x) x^2
    mypower01(1:5)
    ```
 1. $x^y$が返り値の関数`mypower02(x,y)`
    ```{r}
    mypower02 <- function(x, y) x^y
    mypower02(1:5, 3)
    ```
 1. $\sqrt{x}$が返り値の関数`mysqrt(x)`
    ```{r}
    mysqrt <- function(x){
      y <- sqrt(x)
      return(y)
    }
    mysqrt(1:5)
    mypower02(1:5, 0.5)
    ```




# 5.3 プログラムの基本

次の様に条件分岐を書ける．
比較演算子は，`<`，`>`，`<=`，`>=`，`==`，`!=`，等．
要は，TRUE/FALSEが返ってくればなんでもよい．

比較演算子の例
```{r}
a <- 1:5
a > 3
a != 2
```

条件分岐の例
```{r}
a <- 10
b <- 0
if (a > 8){
  b <- 1
}
b
```

次の様に繰り返しを書ける．
```{r}
x <- 0
for (i in 1:10){
  x <- x + i
}
x
```

`in`で指定するのは，適当なベクトルでも良い．

```{r}
x <- NULL
for (s in month.abb){
  x <- paste(x, s)
}
x
```



## 練習問題

 1. $x>1$のとき1を返す関数`myone(x)`
    ```{r}
    myone <- function(x) if(x > 1) return(1)
    myone(1:5)
    ```
 1. $x>1$のとき1，そうてないとき0を返す関数`myindex(x)`
    ```{r}
    myindex <- function(x) as.numeric(x > 1)
    myindex(1:5)
    ```
 1. $|a-b|$が返り値の関数`mydistance(a,b)`
    ```{r}
    mydistance <- function(a,b) abs(a-b)
    mydistance(1:5, seq(5,1,-1))
    ```
 1. $x$に$i$を5回足す
    ```{r}
    x <- 0
    for(i in 1:5) x <- x + i
    x
    ```
 1. $x$に$i$をくっつける
    ```{r}
    x <- c()
    for(i in 1:5) x <- c(x,i)
    x
    ```
 1. $x$までの偶数を全て足す関数`myeven()`
    ```{r}
    myeven <- function(x) sum(seq(0,x,2))
    myeven(20)
    ```
 1. `NA`を除き和をとる関数`myplus()`
    ```{r}
    myplus <- function(x) {
      result <- 0
      for (i in 1:length(x)){
        if(!is.na(x[i])) result <- result + x[i]
      }
      return(result)
    }
    myplus(c(1,2,3,NA,4,5,NA))
    ```
    別解
    ```{r}
    myplus <- function(x) sum(x, na.rm = T)
    myplus(c(1,2,3,NA,4,5,NA))
    ```

# 5.4 落ち穂拾い

省略

