---
title: "Untitled"
author: "taro tsurui"
date: "2019/10/9"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r}
mypower01 <- function(x) {return(x^2)}

mypower01(4)
```

```{r}
mysqrtlog <- function(x) { y <- sqrt(x)
+ return(log(y)) }

mysqrtlog(2.7182818)
```

```{r}
myone <- function(x) {if (x > 1) {return(1)}}

myone(-3)

myone(2)


myindex <- function(x) {if (x>1) {return(1)}
  else {return(0)}}

myindex(-1)



myloop <- function() {
  a <- 0
  for (i in seq(1, 5, by=2)) {
    a <- a + 1
  }
  return(a)
}

myloop()
```

## 7章
```{r}

y <- c(1,1,2,2,3,3,4,4,5,5)
plot(y)

x <- 1:10
y <- 1:10
plot(x,y)

```

```{r}
f <- function(x) {
  cos(x) - log(x)
}

plot(f, 1, 10)
curve(f, 1, 10)
```

```{r}
f <- function(x, y) {
  return(1/(2*pi)*exp(-(x^2+y^2)/2))
}

curve(f(x, 0), -3, 3) 
```

```{r}
y <- c(1,2,2,4,5)

plot(y, main="My Plot", type="h")

plot(y, col="red", pch=3, ann=F)
```


```{r}
x <- runif(10,min=0,max=10)
y <- 1:10
x
plot(x,y, xlim=c(0,10))
hist(x)
```


## 8章

```{r}


```



