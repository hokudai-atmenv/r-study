# R勉強会

## 勉強会の目的

 - プログラミング言語「R」の**基礎**を習得する
 - 「R」による**グラフィックス作成能力**を身に付ける

## Rを使うモチベーション

### 計算

#### 四則演算・関数の利用

基本的には，数式で表せるものはなんでもできる．たとえば，

```math
10\log_{10}\left(2\pi\right)\approx 8
```

は，次の様に打ち込めば計算できる．ここで，上の行は入力，下の行は出力．

```
> 10*log10(2*pi)
[1] 7.981799
```

#### 配列を使った計算

 - Rは，計算には基本的に配列を使う．
 - 配列の計算は，極端に簡単に記述出来る様になっており，しかもそれなりに高速．
 - 繰り返し計算も，配列計算に含まれることが多い．
 
たとえば，ベクトル要素の和
```
> a <- 1:100
> sum(a)
[1] 5050
```

たとえば，行列の掛け算
```
> A <- matrix(1:4, nrow = 2)
> B <- matrix(5:8, nrow = 2)
> A %*% B
     [,1] [,2]
[1,]   23   31
[2,]   34   46
```

なお，同じ様な計算をVBAで行うと，次のようなスクリプトが必要．

```
Sub multMat()
  Dim A(1,1) As Double
  Dim B(1,1) As Double
  Dim C(1,1) As Double
  For ir = 0 To 1
    For ic = 0 To 1
      For j = 0 To 1
        C(ir, ic) = C(ir, ic) + A(ir, j) * B(j, ic)
      Next j
      Sheet1.Cells(ir+1,ic+1) = C(ir, ic)
    Next ic
  Next ir
End Sub
```


### 先人の知恵の利用

 - 世界の誰かが作った関数群が「ライブラリ」として公開されている．
 - 自分で一からプログラミングをする必要はない．
 - 人の作ったものを賢く利用する．

たとえば，CRANリポジトリからのインターネットを経由したライブラリのインストールとインポート
```
> install.packages("tidyverse")
> library(tidyverse)
```

 - ライブラリには，世界の誰かが実験や観察をして得たデータが含まれていることが多い．
 - ライブラリとセットで，解析法を具体例を通して学べる．


たとえば，モルモットの歯芽細胞の成長に関するデータセットの読み込み
```
> data("ToothGrowth")
> head(ToothGrowth)
   len supp dose
1  4.2   VC  0.5
2 11.5   VC  0.5
3  7.3   VC  0.5
4  5.8   VC  0.5
5  6.4   VC  0.5
6 10.0   VC  0.5
```


### グラフィックス

 - Excelではできないグラフも，Rなら描ける
 - 細かい所まで，自分で書式を設定できる．

```{r}
> ggplot(ToothGrowth, aes(dose, len, colour = supp)) +
    geom_jitter(height = 0, width = 0.1) +
    geom_smooth(method = "lm")
```

![ggplotサンプル](img/output-ggplot-sample.png "ggplotサンプル")

### 統計解析

 - そもそも，Rは統計解析をするために開発された言語．
 - ただし，卒論・修論では使わない人も多い．

```
> model <- lm(len ~ dose * supp, data = ToothGrowth)
> summary(model)

Call:
lm(formula = len ~ dose * supp, data = ToothGrowth)

Residuals:
    Min      1Q  Median      3Q     Max 
-8.2264 -2.8462  0.0504  2.2893  7.9386 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)   11.550      1.581   7.304 1.09e-09 ***
dose           7.811      1.195   6.534 2.03e-08 ***
suppVC        -8.255      2.236  -3.691 0.000507 ***
dose:suppVC    3.904      1.691   2.309 0.024631 *  
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 4.083 on 56 degrees of freedom
Multiple R-squared:  0.7296,  Adjusted R-squared:  0.7151 
F-statistic: 50.36 on 3 and 56 DF,  p-value: 6.521e-16
```

### GIS

 - 地理情報データの処理や表示ができる．

```
> sf_tmp <- st_point(c(141.340370,43.075898)) %>% 
>   st_sfc(crs = 4326)
> sf_tmp
Geometry set for 1 feature 
geometry type:  POINT
dimension:      XY
bbox:           xmin: 141.3404 ymin: 43.0759 xmax: 141.3404 ymax: 43.0759
geographic CRS: WGS 84
POINT (141.3404 43.0759)

sf_tmp %>%
  st_transform(2454) %>%
  st_buffer(100) %>%
  st_transform(4326) %>%
  leaflet() %>%
  addTiles() %>%
  addPolygons()
```


![leafletサンプル](img/output-map-sample.png "leafletサンプル")

### レポート作成

 - マークダウン，HTML，PDF，Word，Tex，等，色々なフォーマットでレポートを作成できる．
 - 重要なのは，「処理プロセス」が残り，「再現性」が確保される，つまり，誰が処理をしても同じ結果が得られること．

### その他連携アプリの利用

単に「ライブラリ」の枠を超えた色々な連携が可能．

 - `shiny`: Webアプリケーションを開発できる
 - `reticulate`: Pythonプログラムを実行できる
 - `EZR`: GUI操作で統計解析（医学統計寄り）ができる
 - `Exploratory`: GUI操作で統計解析（機械学習寄り）ができる
 - `Stan`: MCMCアルゴリズムに基づくベイズ統計解析ができる



## 勉強会の参加方法

### 進め方


 - **輪番**で，テキストの該当内容を説明していく．
   - テキストの該当箇所に書いてあることの**概要**を，スクリプト実行結果を示しながら説明する．
   - 説明は，**長くても10分以内**でまとめる．重要ではないところは端折る．
   - 練習問題がある場合には，**全ての模範解答を準備**する．ただし，説明を端折るのはOK．
 - 資料はRマークダウンで作成し，`rmd/`フォルダに保存する．印刷不要．
 - **全員**が，テキストの該当箇所の応用を考える．
   - テキストに書いてあったがよく理解できなかった例
   - 現実のデータやRデータセットに適用できた例
   - 似た処理のはずなのにテキストに書かれた内容ではできない例（原因や解決法も考える）
   - 同じ処理を別の命令やVBAを使ってやってみた例
   - テキストに書いてあった関数の詳細　など
   - テキストの内容を超えても構わない．むしろ推奨．



### Rの準備

 - R( https://www.r-project.org/ )をダウンロード・インストールする．
 - RStudio( https://www.rstudio.com/ )をダウンロード・インストールする．
   - 起動して使える状態になっているか確認すること．
   - とりあえずConsoleに`1+1`と入力して，`2`が返ってくればOK．
   - デザインは使いやすい様に適当に変更する（Tools -> Global Options）．
   - たとえば，
     - Pane Layout: Source(左上)，Console（右上），Environment（左下），Files（右下）
     - Appearance: Monokai
     
   ![](img/rstudio.jpg)
 - Rスクリプトは，いくつか実行方法がある．
   - `Console`に入力して`Enter`
   - `Source`に入力されたコマンドを選択して`Ctrl + Enter`
   - `Source`にコマンドを入力しておいて，画面右上の`Run`

### フォルダの準備

 - `Git`( https://git-scm.com/ )をダウンロード・インストールする
   - `Git`の操作方法はインターネット・本等を参照．
   - 最低限の操作は( https://gitlab.com/jtagusari/git-procedure )にも記載した．
 - `GitLab`( https://gitlab.com/ )のアカウントを作成する．
 - プロジェクトフォルダ全体をダウンロードする．
   - 適当なフォルダで，右クリック->`Git on Bash`を開き，以下のコマンドを実行する．
     ```
     git clone https://gitlab.com/hokudai-atmenv/r-study
     ```
 - ブランチを移動する．
   - ダウンロードしたプロジェクトフォルダで，右クリック->`Git on Bash`を開き，以下のコマンドを実行する．fixブランチに移動する．
     ```
     git pull origin fix
     git checkout fix
     ```

### Rスクリプトの編集

 - 編集前に，ファイルを最新版に変更
   - プロジェクトフォルダで`Git Bash`から以下のコードを実行．
     ```
     git pull origin fix
     ```
 - `rmd/`フォルダ内の該当ファイルを適宜編集する．
   - 見出し（`#`が付いているもの）
   - 説明文（地の分）
   - 箇条書き（`-`などが付いているもの）
   - Rスクリプト（```` ```{r} ````と```` ``` ````で囲まれたもの）
   - 数式（`$`や`$$`で囲まれたもの，文法はmathjaxやtexを参照）
   - 理解できる程度にコメントを入れておくこと．
 - HTMLレポートを出力する．
   - Rstudio上部の`Knit`の`Knit Directory`を`Project Directory`に変更する．
   - `Knit`ボタンを押す．rmdからhtmlが出力される．
   - スクリプトに不備があれば，エラーが出てhtmlは出力されない．この段階でエラーが出るようなら，必要なライブラリがRにインストールされていない可能性がある．
   - たとえば`install.packages("tidyverse")`などの命令でライブラリが導入できる．
   - htmlファイル形式になったファイルは，Webブラウザなどから閲覧できる．
 - ファイルをアップロード
   - まず，次のコードを実行．ローカルリポジトリが更新される`<comment>`は自由に変えてよい．
     ```
     git add .
     git commit -m "<comment>"
     ```
   - 次のコードでファイルを最新版に変更する
     ```
     git pull origin fix
     ```
     - 競合がある際には，`CONFLICT`が表示される．このときは，該当のファイルをRStudio等で開いて，修正する．
   - 次のコードでファイルをアップロードする
     ```
     git push origin fix
     ```

## その他

 - わからないことを決して放置しないこと．
   - インターネット，本，など，いまはRに関する情報は山の様にある．
   - Rのヘルプ，Stack Overflowなどは英語で書かれているが，本質的な情報が多い．
# Rstudioでgithub copilotを使う方法

## githubアカウントを作る

### github educationを申請・許可を得る (https://education.github.com/)
- アカウント作成（メール受信、大学メールを用いる）
- プロフィールを書く
- 学生証の写真を送る (画面表示がThank you for submitting)
- 数日後にメールでgithub education Welcome to Global Campus と送られてくる
- github内のプロフィールでProと表示される



## Rstudio daily builds の最新版をインストール (https://dailies.rstudio.com/) (2023/6/14以降)

- tools → global options → copilot → sign in → アカウント認証（R studioに表示）

# VS code でgithub copilot を使う方法

## github アカウント作成は上記の通り

## VS codeをインストール

### VS codeの設定
- 拡張機能でJapanese Languageをインストール
- github copilot をインストール
- 右下のsign up for github
- 反映されない場合は再起動