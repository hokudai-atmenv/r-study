---
title: "13"
author: "kazuki tanaka"
date: "2021/10/14"
output: html_document
---
# 13章　lubridateによる日付と時刻


## 13.1 用意するもの

　本章では、Rで日付や時刻を扱いやすくするlubridateパッケージに焦点を絞ります。lubridateパッケージはtidyverseに含まれないので、実際のデータにはnycflights13パッケージを使います。
  
```{r}
  library(tidyverse)
　library(lubridate)
  library(nycflights13)
```


## 13.2 日付/時刻の作成

　現在の日付または日付時刻を取得するには、today()またはnow()を用います。

```{r} 
 today()

 now()
```

別途、日付時刻を作るのに3つの方法があります。

　文字列から作成
　個々の日付時刻要素から作成
　既存の日付時刻オブジェクトから作成
　
これらを次に説明します。

＃＃＃13.2.1 文字列から作成
　
　lubridateのヘルパーを使う方法で、使うためには、日付の年、月、日をそれぞれ"y","m","d"で表します。これで日付をパースするlubridate関数の名前がわかります。
　
```{r}
ymd("2017-01-31")

mdy("January 31st,2017")

dmy("31-Jan-2017")
strptime("31-1-2017",format = "%d-%B-%Y")
```

これらの関数は引用符なしの数もとります。
```{r}
 ymd(20170131)
```

日付時刻を作るには、関数名にアンダースコアと「h」、「m」、「s」の組み合わせをつけてパース関数にします。
```{r}
 ymd_hms("2017-01-31 20:11:59")
 mdy_hm("01/31/2017 08:01")
```

タイムゾーンを引数に指定して日付から日付時刻を作ることもできます。

```{r}
 ymd(20170131, tz = "UTC")
```


###13.2.2　個別要素から作成
　
　単一文字列の代わりに、複数列に個別要素がまたがる日付時刻もあります。以下はフライトデータです。
　
```{r}
 flights %>% 
  select(year,month,day,hour,minute)
```

この種の入力から日付時刻を作るには、日付にmake_date()を使います。

```{r}
 flights %>% 
  select(year,month,day,hour,minute) %>% 
  mutate(
    depature = make_datetime(year,month,day,hour,minute)
  )
```

　同じことをflightsの4つの時間列のそれぞれで行います。時刻は少し特殊な形式なので、剰余演算を使って時間と分とを抽出します。
　
```{r}
 make_datetime_100 <- function(year, month, day, time) {
  make_datetime(year, month, day, time %/% 100, time %% 100)
}

flights_dt <- flights %>% 
  filter(!is.na(dep_time), !is.na(arr_time)) %>% 
  mutate(
    dep_time = make_datetime_100(year, month, day, dep_time),
    arr_time = make_datetime_100(year, month, day, arr_time),
    sched_dep_time = make_datetime_100(year, month, day, sched_dep_time),
    sched_arr_time = make_datetime_100(year, month, day, sched_arr_time)
  ) %>% 
  select(origin, dest, ends_with("delay"), ends_with("time"))

flights_dt

```

このデータで1年にわたる出発時刻の分布を可視化します。

```{r}
 flights_dt %>% 
  ggplot(aes(dep_time)) + 
  geom_freqpoly(binwidth = 86400) # 86400 秒 = 1 日
```

1日の分布は次のようになります

```{r}
 flights_dt %>% 
  filter(dep_time < ymd(20130102)) %>% 
  ggplot(aes(dep_time)) + 
  geom_freqpoly(binwidth = 600) # 600 s = 10 minutes
```

日付時間を（ヒストグラムのような）数値として扱うときは、1は1秒を表すので、86400が1日を表します。日付では1は1日を表します。


## 13.2.3　他の型から作成

　日付時刻と日付とを切り替えるときは、as_datetime()とas_date()で行います。
　
```{r}
as_datetime(today())

as_date(now())
```

　日付時刻を「Unixエポック」1970-01-01からのオフセットで与えることもあります。オフセットが秒ならas_datetime()を使い、日ならas_date()を使います。
　
```{r}
as_datetime(60 * 60 * 10)

as_date(365 * 10 + 2)

```

##練習問題

###1
不当な日付を含む文字列をパースするとどうなるか。
 
 答え
 NAとして変換される
 
 ```{r}
 
 ymd(c("2010-10-10","bananas"))
 
 ```

###2
today()のtzoneはどんな役割があるのか。それは重要なのか？

答え
タイムゾーンによって返される値は異なる

```{r}

Sys.timezone()

today(tzone = "")

today(tzone = "UTC")

today(tzone = "GMT")
```


###3
適切なlubridate関数で下記の日付をパースしなさい。

d1 <- "January 1, 2010"
d2 <- "2015-Mar-07"
d3 <- "06-Jun-2017"
d4 <- c("August 19 (2015)", "July 1 (2015)")
d5 <- "12/30/14" # Dec 30, 2014

答え
```{r}
d1 <- "January 1, 2010"
mdy(d1)

d2 <- "2015-Mar-07"
ymd(d2)

d3 <- "06-Jun-2017"
dmy(d3)

d4 <- c("August 19 (2015)", "July 1 (2015)")
mdy(d4)

d5 <- "12/30/14"
mdy(d5)

```

##13.3　日付時刻の要素

　個別要素の取得設定を行うアクセサ関数に焦点を絞ります。
　
###13.3.1 要素を取得する
日付の個々の部分は、year(), month(), mday() , yday() , wday() , hour(), minute(),second()といったアクセサ関数で抽出できます

```{r}
datetime <- ymd_hms("2016-07-08 12:34:56")

year(datetime)

month(datetime)

mday(datetime)

yday(datetime)

wday(datetime)
```

month()とwday()では、label=TRUEと設定して月名や曜日の省略形を返すことができます。
abbr=FALSEとすると完全名が返ります。

```{r}
month(datetime, label = TRUE)

wday(datetime, label = TRUE, abbr = FALSE)
```

wday()を使うと、週末よりも平日の方がフライトが多いことがわかります。

```{r}
flights_dt %>% 
  mutate(wday = wday(dep_time, label = TRUE)) %>% 
  ggplot(aes(x = wday)) +
    geom_bar()
```

1時間のうちの分ごとの平均出発遅延を調べると、20-30,50-60分に出発する便の遅延は他よりも少ない。

```{r}
flights_dt %>% 
  mutate(minute = minute(dep_time)) %>% 
  group_by(minute) %>% 
  summarise(
    avg_delay = mean(arr_delay, na.rm = TRUE),
    n = n()) %>% 
  ggplot(aes(minute, avg_delay)) +
    geom_line()
```

予定出発時間だとそのようなパターンが見られない

```{r}
sched_dep <- flights_dt %>% 
  mutate(minute = minute(sched_dep_time)) %>% 
  group_by(minute) %>% 
  summarise(
    avg_delay = mean(arr_delay, na.rm = TRUE),
    n = n())

ggplot(sched_dep, aes(minute, avg_delay)) +
  geom_line()
```

実際の出発時刻でこのようなパターンが見られるのは、人間が収集したデータの場合、出発便には「ちょうどいい」出発時間への強いバイアスがかかるから。

```{r}
ggplot(sched_dep, aes(minute, n)) +
  geom_line()
```


###13.3.2　丸める

　個別要素をプロットする別の方式は、floor_date(), round_date(), and ceiling_date()で日付をきりのよい時間単位に乗せてしまうこと。
　
```{r}
flights_dt %>% 
  count(week = floor_date(dep_time, "week")) %>% 
  ggplot(aes(week, n)) +
    geom_line()
```

###要素を設定する

　アクセサ関数を使って日付の要素を設定できる
　
```{r}
 (datetime <- ymd_hms("2016-07-08 12:34:56"))


year(datetime) <- 2020
datetime

month(datetime) <- 01
datetime

hour(datetime) <- hour(datetime) + 1
datetime
```

update()で新たな日付時刻を作る方法もある。

```{r}
update(datetime, year = 2020, month = 2, mday = 2, hour = 2)

```

大きすぎる値でもきちんと処理する

```{r}
ymd("2015-02-01") %>% 
  update(mday = 30)

ymd("2015-02-01") %>% 
  update(hour = 400)
```

1年の毎日の時間ごとの便数分布を表示できる。

```{r}
flights_dt %>% 
  mutate(dep_hour = update(dep_time, yday = 1)) %>% 
  ggplot(aes(dep_hour)) +
    geom_freqpoly(binwidth = 300)
```


##練習問題

###1
 1日のフライト時刻の分布は、1年間を通じてどのように変わったか？
 
 答え
 　あまり変わっていなさそう
 　強いていえば2月が少なそう
 　
```{r}
 flights_dt %>%
  select(dep_time) %>% 
  filter(!is.na(dep_time)) %>%
  mutate(dep_hour = update(dep_time, yday = 1),
         month = factor(month(dep_time))) %>% 
  ggplot(., aes(dep_hour, color = month)) +
  geom_freqpoly(binwidth = 60 * 60) 
```

###2
 dep_time、sched_dep_timeおよびdep_delayを比較しなさい。一貫した変化はあるのか。
 ```{r}
 flights_dt %>%  mutate(dep_time_ = sched_dep_time + dep_delay * 60) %>%  filter(dep_time_ != dep_time) %>%  select(dep_time_, dep_time, sched_dep_time, dep_delay)
 ```

###3
 air_timeを出発から到着までの時間差を比較しなさい。
 
 答え
 ずれはある
 
```{r}
flights_dt %>%  select(origin, dest, arr_time, dep_time, air_time) %>%   mutate(flight_duration = as.numeric(arr_time - dep_time),         air_time_mins = air_time,         diff = flight_duration - air_time_mins)
```

###4
 平均遅延時間は1日の間にどのように変わりますか？dep_timeまたはsched_dep_timeのいずれを使うべきなのか？
 
  答え
  　dep_timeはフライトを遅らせると、それが積み重なって、遅延時間が大きくなる傾向があり、フライトを遅らせると時間が遅れますが、sched_dep_timeは、フライトをスケジュールする人にとって重要な指標
  　
```{r}
 flights_dt %>%  select(sched_dep_time, dep_delay) %>%   mutate(sched_dep_hour = hour(sched_dep_time)) %>%  group_by(sched_dep_hour) %>%  summarise(dep_delay = mean(dep_delay, na.rm = TRUE)) %>%  ggplot(aes(sched_dep_hour, dep_delay)) +  geom_point() +  geom_smooth()
```


###5
 遅延の可能性を最小限に抑えたい場合は、何曜日に出発すべきか。
 
 答え
 　土曜日
 　
 ```{r}
  flights_dt %>%  select(sched_dep_time, dep_delay) %>%   mutate(dow = wday(sched_dep_time)) %>%  group_by(dow) %>%  summarise(dep_delay = mean(dep_delay))
 ```

###6
 diamonds$caratとflights$sched_dep_timeの分布が似ているのはなぜか？
 
 答え
  ある閾値を設定し、その点を超えると、繰りあげ処理が行なわれているから分布が似ている
  
  
###7
 20〜30分および50〜60分でのフライトは出発遅延が少ない。それよりも早く出発する便による影響という私の仮説を確認しなさい。
 
 　
 ```{r}
 flights_dt %>%  mutate(minute = minute(dep_time),        early = dep_delay < 0) %>%  group_by(minute) %>%  summarise(early = mean(early, na.rm = TRUE),            n = n()) %>%  ggplot(aes(minute, early)) +  geom_line()
 ```

##13.4 タイムスパン

　タイムスパンを表す3つの重要なクラス
　
　durationは正確に秒数で表す
　periodは週や月のような人が使う単位で表す
　intervalは開始点と終了点で表す
　
###13.4.1 duration（期間）

　durationには便利なコンストラクタがたくさんある
　
```{r}
dseconds(15)

dminutes(10)

dhours(c(12, 24))

dweeks(3)

dyears(1)
```

durationは常に秒で記録する
durationは足したりかけたりできる

```{r}
2 * dyears(1)

dyears(1) + dweeks(12) + dhours(15)


#durationを日に足したり引いたりできる

tomorrow <- today() + ddays(1)
last_year <- today() - dyears(1)


#サマータイムの場合1日が23時間なので、ずれる

one_pm <- ymd_hms("2016-03-12 13:00:00", tz = "America/New_York")

one_pm

one_pm + ddays(1)

```

###13.4.2 period（時期）

この問題の解決のため、periodが作られた。「人間」の時間感覚に合わせる。

```{r}
one_pm
one_pm + days(1)
```

periodにも多くのコンストラクタ関数がある。

```{r}
seconds(15)

minutes(10)

hours(c(12, 24))

days(7)

months(1:6)

weeks(3)

years(1)
```

足したりかけたりできる

```{r}
10 * (months(6) + days(1))

days(50) + hours(25) + minutes(2)
```

日付にも足せる

```{r}
# 閏年
ymd("2016-01-01") + dyears(1)

ymd("2016-01-01") + years(1)


# サマータイム
one_pm + ddays(1)

one_pm + days(1)
```

フライトにも適用。ニューヨーク出発時刻よりも早い時刻に到着する便がある。

```{r}
flights_dt %>% 
  filter(arr_time < dep_time)
```

夜行便にはday(1)を足すとに付けのずれをなくせる

```{r}
flights_dt <- flights_dt %>% 
  mutate(
    overnight = arr_time < dep_time,
    arr_time = arr_time + days(overnight * 1),
    sched_arr_time = sched_arr_time + days(overnight * 1)
  )
```

###13.4.3 interval（間隔）

years(1)/days(1)の正確な測定結果がほしいとき、intervalを使う

```{r}
 next_year <- today() + years(1)
(today() %--% next_year) / ddays(1)
```


##　練習問題

##1
 months()があるのにdmonths()がないのはなぜか。

 答え
 　d～()はdurationを表す関数であり、各月の日数が異なるため、dmonths()がない
 　
 　
##2
 Rを学び始めたばかりの人にdays(overnight * 1)を説明しなさい。
 　
 　

##3
 2015年の毎月の最初の日を示す日付のベクトルを作成しなさい。また、今年の毎月の最初の日を示す日付のベクトルを作成します。

　答え
　　基準日を作成し、それに1月ごとに足していけば作成できる。
　　
```{r}
ymd("2015-01-01") + months(0:11)
```

##4
 誕生日(日付として)を与えて、何歳かを年数で返す関数を書きなさい。

##5
 (today() %--% (today() + years(1)) / months(1)はなぜうまく動かないのか。
 
 答え
 　)が足りないから
 
```{r}
(today() %--% (today() + years(1)))/months(1)
```

##13.5 タイムゾーン

　現在のタイムゾーンをRがどのように判断しているか
　
```{r}
Sys.timezone()
```
 　
 次の3つは同じ時刻を表す

```{r}
(x1 <- ymd_hms("2015-06-01 12:00:00", tz = "America/New_York"))

(x2 <- ymd_hms("2015-06-01 18:00:00", tz = "Europe/Copenhagen"))

(x3 <- ymd_hms("2015-06-02 04:00:00", tz = "Pacific/Auckland"))
```

引き算すると同じ時間だとわかる
```{r}
x1-x2

x1-x3
```

特に指定しないと、lubridateは常にUTCを使う

```{r}
x4 <- c(x1, x2, x3)
x4
```


タイムゾーンを変えるには次の2つがある

時刻はそのままにして表示方法を変える

```{r}
x4a <- with_tz(x4, tzone = "Australia/Lord_Howe")
x4a

x4a - x4
```

基盤となる時刻を変更する

```{r}
x4b <- force_tz(x4, tzone = "Australia/Lord_Howe")
x4b

x4b - x4
```