---
title: "R-Tips_chap15"
author: "Syu Sakai"
date: "2021/10/4"
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
    number_sections: false
editor_options: 
  chunk_output_type: console
---

# 第15章 データハンドリング

## 15.1 データフレームとは

`data.frame`クラスを持つリスト.

数値ベクトルや文字ベクトル，因子ベクトル（文字ベクトル）などの異なる型のデータをまとめて一つの変数として持つ.  

データフレームの各行・列はラベルを必ず持ち，ラベルによる操作が可能．


## 15.2 データフレームの作成

データフレームの作成方法はいくつかある．

### 1. ベクトルからデータフレームを作成する

基本の作成方法．  
いくつかのデータをベクトルで用意しておき，それらを`data.frame()`でデータフレームに変換する．  
ベクトル以外に行列による作成も可能．

```{r}
id     <- c(1, 2, 3, 4, 5)           # ID
sex    <- c("F", "F", "M", "M", "M") # 性別
height <- c(160, 165, 170, 175, 180) # 身長
weight <- c(50, 65, 60, 55, 70)      # 体重
(MYDATA <- data.frame(ID=id, SEX=sex, HEIGHT=height, WEIGHT=weight))
class(MYDATA)
```

### 2. テキストファイルからデータフレームを作成する

今回は本Rmdファイルと同じ作業ディレクトリ内に存在するフォルダから，テキストファイルを選択しデータフレームを作成する場合を考える．

 - `read.table()`を用い，テキストファイルを指定する．
 - 引数`header`で列名の記述があるかを指定
 - 引数`sep`で区切り文字を指定．
 
```{r}
(MYDATA <- read.table("data/mydata.txt", header=TRUE, sep=""))
```

異なる作業ディレクトリのファイルを指定する場合

 1. パスごとテキストファイルを指定する．
 
    例えば，`read.table("C:/Users/SyuSakai/Desktop/data/mydata.txt", header=TRUE, sep="")`
   
 1. `setwd()`  により， Rの作業ディレクトリを変更する．

### 3. 様々な形式のテキストファイルからデータフレームを作成する

CSVファイルはよく使う．

```{r}
read.csv("data/mydata.csv")
```

## 15.3 データフレームの閲覧と集計

```{r}
head(MYDATA,2)   # 先頭2行のみ閲覧
summary(MYDATA)  # 要素それぞれの特徴（最大値，型など）を表示
plot(MYDATA)     # データフレームの各変数について対散布図のプロット
```

## 15.4 データの編集・加工方法

以下の`MYDATA1`と`MYDATA2`を元にデータの編集・加工を行う．

```{r}
id     <- c(1, 2, 3, 4, 5)           # ID
sex    <- c("F", "F", "M", "M", "M") # 性別
height <- c(160, 165, 170, 175, 180) # 身長
weight <- c(50, 65, 60, 55, 70)      # 体重

(MYDATA1 <- data.frame(ID=id,SEX=sex, HEIGHT=height))
(MYDATA2 <- data.frame(ID=id[1:4], WEIGHT=weight[1:4]))
```

### 1. データへのアクセス方法

 1. `subset()`により一部の変数だけを確認できる．

    ```{r}
    subset(MYDATA1, HEIGHT>165)  # 「変数HEIGHTが165よりも大きい」行を表示
    ```
    
 1. 1列目と2列目以外を表示．
    
    ```{r}
    MYDATA1[,c(-1,-2)]   # MYDATAから削除された訳ではない
    ```

 1. `NULL`を代入して変数を削除
 
    ```{r}
    MYDATA1$SEX <- NULL
    MYDATA1
    ```

### 2. 行や列の情報の取得・データの並び替え

`MYDATA2`を`WEIGHT`について昇順に並び替える．

```{r}
MYDATA2
x <- order(MYDATA2$WEIGHT)            # WEIGHTを昇順に並べた場合の行番号リストをxに代入
x
(MYDATA2 <- MYDATA2[x,])              # データの並び替え
```

このままでは行ラベルがバラバラなのでラベルを付け直す．

```{r}
rownames(MYDATA2) <- 1:nrow(MYDATA2)   # xの行ラベルに1:nrow(……)を指定
MYDATA2
```

```{r include=FALSE}
id     <- c(1, 2, 3, 4, 5)           # ID
sex    <- c("F", "F", "M", "M", "M") # 性別
height <- c(160, 165, 170, 175, 180) # 身長
weight <- c(50, 65, 60, 55, 70)      # 体重

(MYDATA1 <- data.frame(ID=id,SEX=sex, HEIGHT=height))
(MYDATA2 <- data.frame(ID=id[1:4], WEIGHT=weight[1:4]))
```

### 3. データの加工・編集方法

 1. 関数`transform()`
 
    新たな変数列の追加． 

    ```{r}
    MYDATA1          # WEIGHTを追加したい...
    
    weight <- c(50,65,60,55,70)
    MYDATA3 <- transform(MYDATA1, WEIGHT=weight) # MYDATA$WEIGHT <- weightでも可
    MYDATA3
    ```

 1. 関数`rbind()`,`cbind()`,`data.frame()`

    - `rbind()`                列名がすべて同じデータフレームを縦に結合
    - `cbind()`,`data.frame()` 行数がすべて同じデータフレームを横に結合
    
    ```{r}
    cbind(MYDATA1[,c(1,2)], MYDATA1[,c(1,3)])
    ```

 1. 関数`ifelse(<条件式>, <TRUEで返す値>, <FALSEで返す値>)`

    条件に当てはまる行のみに処理を施す．

    ```{r}
    MYDATA$WEIGHT <- ifelse(MYDATA$SEX == "F", NA, MYDATA$WEIGHT) # 女性の体重を隠す
    MYDATA
    ```

```{r include=FALSE}
id     <- c(1, 2, 3, 4, 5)           # ID
sex    <- c("F", "F", "M", "M", "M") # 性別
height <- c(160, 165, 170, 175, 180) # 身長
weight <- c(50, 65, 60, 55, 70)      # 体重
MYDATA <- data.frame(ID=id, SEX=sex, HEIGHT=height, WEIGHT=weight)
```

## 15.5 パッケージdplyr

データサイエンス第3章で詳しく扱う．省略

## 15.6 ファイルへのデータ出力

Rで作成したデータフレームを外部ファイルに出力する際には，関数`write.table()`を使用する．  
同じ作業ディレクトリの「Data」フォルダに，`output.txt`として出力する．

```{r}
write.table(MYDATA, file = "data/output.txt", row.names=FALSE, quote=FALSE, append=FALSE)
```

他にも，`write.csv()`によってCSVファイルに出力することもできる．

```{r}
write.csv(MYDATA, file = "data/output.csv")
```

ファイルそのものを直接編集しない場合，`save()`で記録して`load()`で呼び出す．  
このとき作成されるdataファイルはテキストデータではないため，エディタで見ることはできない．

```{r error=T}

save(MYDATA, file = "data/MYDATA.data")
rm(MYDATA)   # MYDATAを削除
MYDATA

load("data/MYDATA.data")  #データの呼び出し
MYDATA
```

## 15.7 落穂拾い

関数`scan()`によってデータを読み込むこともできる．  
多くのデータを取り込む場合や複雑な規則でデータの読み込みを行いたい場合は`scan()`を使用した方がよいかもしれない.(?)  
`scan()`はデータをベクトルとして読み込んでいる．  
そのためデータフレームとして読み込む場合には．まずデータを行列として読み込んだ後，データフレームに変換する必要がある．

```{r}
MYDATA_scanned <- data.frame(
  matrix(scan("data/mydata.txt", what = "", skip = 1, nlines = 6), 5, by = T))
MYDATA_scanned
```

