---
title: "Graphics_chap10"
author: "Kazuya Okada"
date: "2021/10/27"
output: html_document
---

# 10章　凡例

```{r}
library(ggplot2)
library(grid)
```

## 10.1 凡例を消す

グラフから凡例を消したい。 

```{r}
# 基本プロット（凡例付き）
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()
p

#fillの凡例を消す
p + guides(fill=FALSE)
```

scaleでguide_FALSEを指定しても凡例を消せる。

```{r}
p + scale_fill_discrete(guide=FALSE)
```

凡例を消す別の方法としてテーマシステムを使うこともできる。複数のエステティックマッピング（例えばcolorとshape）を使っている場合には、この方法ですべての凡例を消すことが出来る。

```{r}
p + theme(legend.position="none")
```

## 10.2 凡例の位置を変える

右側（デフォルト）以外の位置に凡例を動かしたい

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot() + scale_fill_brewer(palette="Pastel2")

p + theme(legend.position="top")
```

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot() + scale_fill_brewer(palette="Pastel2")

p + theme(legend.position=c(1,0))
```

凡例ボックスのどの部分をlegend.positionに配置するかはlegend.justificationで指定できる

```{r}
p + theme(legend.position=c(1,0),legend.justification=c(1,0))
```

凡例をそれ以外と区別するために不透明な境界線を付ける場合

```{r}
p + theme(legend.position=c(.85,.2)) + theme(legend.background=element_rect(fill="white",colour="black"))
```

要素の周りの境界線を取り除くなら

```{r}
p + theme(legend.position=c(.85,.2)) + theme(legend.background=element_blank()) + theme(legend.key=element_blank())
```

## 10.3 凡例の項目順を変える

凡例の項目順を変えたい。

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()
p

# 項目順の変更
p + scale_fill_discrete(limits=c("trt1","trt2","ctrl"))

```

前の例で、groupはエステティック属性fillにマッピングしたが、この場合、デフォルトでは色相環上で等間隔に配置された色にファクタレベルをマッピングするscale_fill_discrete()が使われる。別のscale_fill_xxx()を使うこともできる。

```{r}
p + scale_fill_grey(start=.5,end=1,limits=c("trt1","trt2","ctrl"))
```

また、RColourBrewerのパレットを使うこともできる。

```{r}
p + scale_fill_brewer(palette="Pastel2",limits=c("trt1","trt2","ctrl"))
```

## 10.4 凡例の項目順を反転させる

凡例の項目順を反転させたい。

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()
p

# 凡例の順序を反転させる
p + guides(fill=guide_legend(reverse=TRUE))
```

## 10.5 凡例のタイトルを変更する

凡例のタイトルテキストを変更したい。

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()
p

# 凡例のタイトルを"Condition"に設定
p + labs(fill="Condition")

```


## 10.6 凡例タイトルの体裁を変更する

凡例のタイトルテキストの体裁を変更したい。

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()

p + theme(legend.title=element_text(face="italic",family="Times",colour="red",size=14))
```

## 10.7 凡例タイトルを消す

凡例タイトルを消したい。

```{r}
ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot() + guides(fill=guide_legend(title=NULL))
```

## 10.8 凡例内のラベルを変更する

凡例内のテキストラベルを変更したい。

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()

# 凡例ラベルを変更
p + scale_fill_discrete(labels=c("Control","Treatment 1","Treatment 2")) 
```

```{r}

```


## 10.9 凡例ラベルの体裁を変更する

凡例ラベルの体裁を変更したい。

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()

# 凡例ラベルの体裁を変更
p + theme(legend.text=element_text(face="italic",family="Times",colour="red",size=14))

```

## 10.10 複数行テキストをラベルに使う

複数行のテキストを凡例ラベルに使いたい。

```{r}
p <- ggplot(PlantGrowth,aes(x=group,y=weight,fill=group)) + geom_boxplot()

# 複数行のラベル
p + scale_fill_discrete(labels=c("Control","Type 1\ntreatment","Type 2\ntreatment"))

```

行が重なる問題に対処するには、theme()を使って凡例キーの高さを増やし、行間スペースを減らす。これにはgridパッケージのunit()関数を使って高さを指定する必要がある。

```{r}
p + scale_fill_discrete(labels=c("Control","Type 1\ntreatment","Type 2\ntreatment")) + theme(legend.text=element_text(lineheight=.8),legend.key.height=unit(1,"cm"))
```